const matriz = new Array(4);
const objetos = new Array();
const aux = new Array();
const intentos = 0;
var imgDOM = null;


function random(){
    return Math.random()-0.5;
}

function crearMatriz() {

    var cont = 0;
    
    for (var i = 0; i < 4; i++) {
        matriz[i] = new Array(4);
    }
 
    for (var fila = 0; fila < 4; fila++) {
        for (var columna = 0; columna < 4; columna++) {
          matriz[fila][columna] = objetos[cont];
          cont++;
        }
    }

    
}
function delay(tiempo){
    return new Promise(resolve => setTimeout(resolve,tiempo));
}

function check1(fila,columna,id){
    var data = {fila:fila,columna:columna,id:id};
    aux.push(data);
}

async function check3(id){
    //console.log('Esperando');
    await delay(2500);
    objeto1.estado=false;   
    objeto2.estado=false;  
    imgDOM = document.getElementById(id);      
    imgDOM.setAttribute('src','./img/acertijo.jpg');
    imgDOM = document.getElementById(aux[0].id);        
    imgDOM.setAttribute('src','./img/acertijo.jpg');
    aux.length = 0;
    
    //console.log('fin');
}

function check2(fila,columna,id){

    objeto1 = matriz[(aux[0]).fila][(aux[0]).columna];
    objeto2 = matriz[fila][columna];

    if(objeto1.id === objeto2.id)
        aux.length=0;        
    else
        check3(id);

    
}


function crearObjetos(){

    objetos[0] = {img:'img/chrome.svg',id:0,estado:false};
    objetos[1] = {img:'img/chrome.svg',id:0,estado:false};
    objetos[2] = {img:'img/facebook.svg',id:1,estado:false};
    objetos[3] = {img:'img/facebook.svg',id:1,estado:false};
    objetos[4] = {img:'img/firefox.svg',id:2,estado:false};
    objetos[5] = {img:'img/firefox.svg',id:2,estado:false};
    objetos[6] = {img:'img/google-icon.svg',id:3,estado:false};
    objetos[7] = {img:'img/google-icon.svg',id:3,estado:false};
    objetos[8] = {img:'img/html-5.svg',id:4,estado:false};
    objetos[9] = {img:'img/html-5.svg',id:4,estado:false};
    objetos[10] = {img:'img/instagram-icon.svg',id:5,estado:false};
    objetos[11] = {img:'img/instagram-icon.svg',id:5,estado:false};
    objetos[12] = {img:'img/javascript.svg',id:6,estado:false};
    objetos[13] = {img:'img/javascript.svg',id:6,estado:false};
    objetos[14] = {img:'img/opera.svg',id:7,estado:false};
    objetos[15] = {img:'img/opera.svg',id:7,estado:false};

    objetos.sort(random);
    console.log(objetos);
}

function seleccion(fila,columna,id){

    if((matriz[fila][columna]).estado != true){

        imgDOM = document.getElementById(id);        
        imgDOM.setAttribute('src',(matriz[fila][columna]).img);
        (matriz[fila][columna]).estado = true;
    
        
        if(aux.length == 0)
            check1(fila,columna,id);
        else{
            check2(fila,columna,id);
        }  
            
    }
 

}

/* -------------------------------------Programa------------------------------------- */

crearObjetos();
crearMatriz();

